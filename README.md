[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://choosealicense.com/licenses/mit/)
[![GPLv3 License](https://img.shields.io/badge/License-CERN_OHL_P_2.0-green.svg)](https://cern-ohl.web.cern.ch/)

# HERMES

HERMES est un acronyme pour "Hybrid Emitting Receiving Modulation Educational Solution". Ce projet a pour objectif de réaliser une maquette  permettant de visualiser des modulations numériques.

Ce projet comporte une partie software, écrite en VHDL pour un FGPA MachXO2 de Lattice Semiconductors, et une partie hardware.

## Roadmap HERMES Tx

- Mise à jour licences

- Correction de bugs restants


## License

Ce projet possède deux licenses :

### Partie software :

[MIT](https://choosealicense.com/licenses/mit/)

### Partie hardware :

[CERN-OHL-P-2.0](https://cern-ohl.web.cern.ch/)

