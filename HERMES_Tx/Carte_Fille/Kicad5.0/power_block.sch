EESchema Schematic File Version 4
LIBS:carte_fille-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "Étage conversion modulateur carte HERMES"
Date "2023-11-08"
Rev "1.1"
Comp "IUT GEII"
Comment1 "LAFFEZ J. - LOUISE K. - ANDRIEU J."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 654F53AE
P 1500 1200
AR Path="/654F53AE" Ref="J?"  Part="1" 
AR Path="/654E77F4/654F53AE" Ref="J1"  Part="1" 
F 0 "J1" H 1420 875 50  0000 C CNN
F 1 "Conn_01x03" H 1420 966 50  0000 C CNN
F 2 "Connector:Banana_Jack_3Pin" H 1500 1200 50  0001 C CNN
F 3 "~" H 1500 1200 50  0001 C CNN
	1    1500 1200
	-1   0    0    1   
$EndComp
Wire Wire Line
	1700 1100 1750 1100
Text GLabel 1850 1100 2    50   Input ~ 0
+5V
Wire Wire Line
	1700 1300 1750 1300
Text GLabel 1850 1300 2    50   Input ~ 0
-5V
Wire Wire Line
	1700 1200 2150 1200
$Comp
L power:GND #PWR?
U 1 1 654F53BA
P 2150 1350
AR Path="/654F53BA" Ref="#PWR?"  Part="1" 
AR Path="/654E77F4/654F53BA" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 2150 1100 50  0001 C CNN
F 1 "GND" H 2155 1177 50  0000 C CNN
F 2 "" H 2150 1350 50  0001 C CNN
F 3 "" H 2150 1350 50  0001 C CNN
	1    2150 1350
	1    0    0    -1  
$EndComp
$Comp
L projet:AP7370-WW U?
U 1 1 654F53C0
P 3900 1100
AR Path="/654F53C0" Ref="U?"  Part="1" 
AR Path="/654E77F4/654F53C0" Ref="U1"  Part="1" 
F 0 "U1" H 3900 1415 50  0000 C CNN
F 1 "AP7370-WW" H 3900 1324 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 3850 1400 50  0001 C CNN
F 3 "" H 3850 1400 50  0001 C CNN
	1    3900 1100
	1    0    0    -1  
$EndComp
Text GLabel 3200 1100 0    50   Input ~ 0
+5V
Wire Wire Line
	3200 1100 3350 1100
$Comp
L Device:C C?
U 1 1 654F53C9
P 3350 1400
AR Path="/654F53C9" Ref="C?"  Part="1" 
AR Path="/654E77F4/654F53C9" Ref="C1"  Part="1" 
F 0 "C1" H 3465 1446 50  0000 L CNN
F 1 "1.0u" H 3465 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 3388 1250 50  0001 C CNN
F 3 "~" H 3350 1400 50  0001 C CNN
	1    3350 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 1100 3500 1100
Connection ~ 3350 1100
Wire Wire Line
	3350 1250 3350 1100
Wire Wire Line
	4300 1100 4450 1100
Wire Wire Line
	4450 1100 4450 1250
Wire Wire Line
	4450 1100 4600 1100
Connection ~ 4450 1100
Wire Wire Line
	3900 1300 3900 1700
Wire Wire Line
	3350 1550 3350 1700
Wire Wire Line
	3350 1700 3900 1700
$Comp
L Device:C C?
U 1 1 654F53DA
P 4450 1400
AR Path="/654F53DA" Ref="C?"  Part="1" 
AR Path="/654E77F4/654F53DA" Ref="C2"  Part="1" 
F 0 "C2" H 4565 1446 50  0000 L CNN
F 1 "1.0u" H 4565 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4488 1250 50  0001 C CNN
F 3 "~" H 4450 1400 50  0001 C CNN
	1    4450 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 1550 4450 1700
Wire Wire Line
	4450 1700 3900 1700
Connection ~ 3900 1700
Wire Wire Line
	3900 1700 3900 1850
$Comp
L power:GND #PWR?
U 1 1 654F53E5
P 3900 1850
AR Path="/654F53E5" Ref="#PWR?"  Part="1" 
AR Path="/654E77F4/654F53E5" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 3900 1600 50  0001 C CNN
F 1 "GND" H 3905 1677 50  0000 C CNN
F 2 "" H 3900 1850 50  0001 C CNN
F 3 "" H 3900 1850 50  0001 C CNN
	1    3900 1850
	1    0    0    -1  
$EndComp
Text GLabel 4600 1100 2    50   Input ~ 0
+3V
$Comp
L power:PWR_FLAG #FLG?
U 1 1 654F53EC
P 1750 1100
AR Path="/654F53EC" Ref="#FLG?"  Part="1" 
AR Path="/654E77F4/654F53EC" Ref="#FLG01"  Part="1" 
F 0 "#FLG01" H 1750 1175 50  0001 C CNN
F 1 "PWR_FLAG" H 1750 1274 50  0000 C CNN
F 2 "" H 1750 1100 50  0001 C CNN
F 3 "~" H 1750 1100 50  0001 C CNN
	1    1750 1100
	1    0    0    -1  
$EndComp
Connection ~ 1750 1100
Wire Wire Line
	1750 1100 1850 1100
$Comp
L power:PWR_FLAG #FLG?
U 1 1 654F53F4
P 1750 1300
AR Path="/654F53F4" Ref="#FLG?"  Part="1" 
AR Path="/654E77F4/654F53F4" Ref="#FLG02"  Part="1" 
F 0 "#FLG02" H 1750 1375 50  0001 C CNN
F 1 "PWR_FLAG" H 1750 1473 50  0000 C CNN
F 2 "" H 1750 1300 50  0001 C CNN
F 3 "~" H 1750 1300 50  0001 C CNN
	1    1750 1300
	-1   0    0    1   
$EndComp
Connection ~ 1750 1300
Wire Wire Line
	1750 1300 1850 1300
Wire Wire Line
	2150 1200 2150 1350
$Comp
L power:PWR_FLAG #FLG?
U 1 1 654F53FD
P 2150 1200
AR Path="/654F53FD" Ref="#FLG?"  Part="1" 
AR Path="/654E77F4/654F53FD" Ref="#FLG03"  Part="1" 
F 0 "#FLG03" H 2150 1275 50  0001 C CNN
F 1 "PWR_FLAG" V 2150 1328 50  0000 L CNN
F 2 "" H 2150 1200 50  0001 C CNN
F 3 "~" H 2150 1200 50  0001 C CNN
	1    2150 1200
	0    1    1    0   
$EndComp
Connection ~ 2150 1200
Text GLabel 5300 1100 0    50   Input ~ 0
+3V
Wire Wire Line
	5300 1100 5450 1100
$Comp
L Device:R R?
U 1 1 654F5406
P 5600 1100
AR Path="/654F5406" Ref="R?"  Part="1" 
AR Path="/654E77F4/654F5406" Ref="R1"  Part="1" 
F 0 "R1" V 5393 1100 50  0000 C CNN
F 1 "10k" V 5484 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5530 1100 50  0001 C CNN
F 3 "~" H 5600 1100 50  0001 C CNN
	1    5600 1100
	0    1    1    0   
$EndComp
$Comp
L Device:R_POT RV?
U 1 1 654F540D
P 5900 1400
AR Path="/654F540D" Ref="RV?"  Part="1" 
AR Path="/654E77F4/654F540D" Ref="RV1"  Part="1" 
F 0 "RV1" H 5830 1446 50  0000 R CNN
F 1 "1k" H 5830 1355 50  0000 R CNN
F 2 "Potentiometer_SMD:Potentiometer_Bourns_3214G_Horizontal" H 5900 1400 50  0001 C CNN
F 3 "~" H 5900 1400 50  0001 C CNN
	1    5900 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 1550 5900 1600
Wire Wire Line
	5900 1600 6100 1600
Wire Wire Line
	6100 1600 6100 1400
Wire Wire Line
	6100 1400 6050 1400
Connection ~ 5900 1600
Wire Wire Line
	5900 1600 5900 1700
Wire Wire Line
	5900 1250 5900 1100
Wire Wire Line
	5900 1100 5750 1100
$Comp
L power:GND #PWR?
U 1 1 654F541C
P 5900 1700
AR Path="/654F541C" Ref="#PWR?"  Part="1" 
AR Path="/654E77F4/654F541C" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 5900 1450 50  0001 C CNN
F 1 "GND" H 5905 1527 50  0000 C CNN
F 2 "" H 5900 1700 50  0001 C CNN
F 3 "" H 5900 1700 50  0001 C CNN
	1    5900 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 1100 6050 1100
Connection ~ 5900 1100
Text GLabel 6050 1100 2    50   Input ~ 0
Vr
$EndSCHEMATC
