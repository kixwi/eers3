EESchema Schematic File Version 4
LIBS:carte_fille-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "Étage conversion modulateur carte HERMES"
Date "2023-11-08"
Rev "1.1"
Comp "IUT GEII"
Comment1 "LAFFEZ J. - LOUISE K. - ANDRIEU J."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MAX5190:max5190 U?
U 1 1 654EFE0F
P 3900 1500
AR Path="/654EFE0F" Ref="U?"  Part="1" 
AR Path="/654E781B/654EFE0F" Ref="U2"  Part="1" 
F 0 "U2" H 3700 1400 50  0000 C CNN
F 1 "max5190" H 4300 1400 50  0000 C CNN
F 2 "Package_SO:QSOP-24_3.9x8.7mm_P0.635mm" H 3900 1850 50  0001 C CNN
F 3 "" H 3900 1850 50  0001 C CNN
	1    3900 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 2250 3500 2250
Wire Wire Line
	2200 2350 3500 2350
Wire Wire Line
	2200 2450 3500 2450
Wire Wire Line
	2200 2550 3500 2550
Wire Wire Line
	2200 2750 3500 2750
Wire Wire Line
	3500 2850 2200 2850
Wire Wire Line
	2200 2950 3500 2950
Wire Wire Line
	2200 2650 3500 2650
Wire Wire Line
	3850 3150 3850 3300
Wire Wire Line
	4150 3300 4150 3150
Wire Wire Line
	3850 3300 3950 3300
Wire Wire Line
	4050 3150 4050 3300
Connection ~ 4050 3300
Wire Wire Line
	4050 3300 4150 3300
Wire Wire Line
	3950 3150 3950 3300
Connection ~ 3950 3300
Wire Wire Line
	3950 3300 4000 3300
Wire Wire Line
	4000 3300 4000 3450
Connection ~ 4000 3300
Wire Wire Line
	4000 3300 4050 3300
$Comp
L power:GND #PWR?
U 1 1 654EFE2A
P 4000 3450
AR Path="/654EFE2A" Ref="#PWR?"  Part="1" 
AR Path="/654E781B/654EFE2A" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 4000 3200 50  0001 C CNN
F 1 "GND" H 4005 3277 50  0000 C CNN
F 2 "" H 4000 3450 50  0001 C CNN
F 3 "" H 4000 3450 50  0001 C CNN
	1    4000 3450
	1    0    0    -1  
$EndComp
Text GLabel 4000 1250 1    50   Input ~ 0
+3V
Wire Wire Line
	4000 1250 4000 1400
Wire Wire Line
	4000 1400 3950 1400
Wire Wire Line
	3950 1400 3950 1550
Wire Wire Line
	4000 1400 4050 1400
Wire Wire Line
	4050 1400 4050 1550
Connection ~ 4000 1400
Wire Wire Line
	2550 1850 3500 1850
Wire Wire Line
	3500 1950 2550 1950
Wire Wire Line
	2550 2050 3500 2050
Wire Wire Line
	3500 2150 2550 2150
$Comp
L TLV3542IDR:TLV3542IDR U?
U 1 1 654EFE4B
P 6700 1950
AR Path="/654EFE4B" Ref="U?"  Part="1" 
AR Path="/654E781B/654EFE4B" Ref="U4"  Part="1" 
F 0 "U4" H 6250 2500 50  0000 C CNN
F 1 "TLV3542IDR" H 7000 2500 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 6700 1950 50  0001 L BNN
F 3 "" H 6700 1950 50  0001 L BNN
F 4 "Texas Instruments" H 6700 1950 50  0001 L BNN "MF"
F 5 "\\nDual , 200MHz, RRIO, CMOS Operational Amplifier\\n" H 6700 1950 50  0001 L BNN "Description"
F 6 "SOIC-8 Texas Instruments" H 6700 1950 50  0001 L BNN "Package"
F 7 "None" H 6700 1950 50  0001 L BNN "Price"
F 8 "https://www.snapeda.com/parts/TLV3542IDR/Texas+Instruments/view-part/?ref=snap" H 6700 1950 50  0001 L BNN "SnapEDA_Link"
F 9 "TLV3542IDR" H 6700 1950 50  0001 L BNN "MP"
F 10 "https://www.snapeda.com/api/url_track_click_mouser/?unipart_id=802337&manufacturer=Texas Instruments&part_name=TLV3542IDR&search_term=None" H 6700 1950 50  0001 L BNN "Purchase-URL"
F 11 "In Stock" H 6700 1950 50  0001 L BNN "Availability"
F 12 "https://www.snapeda.com/parts/TLV3542IDR/Texas+Instruments/view-part/?ref=eda" H 6700 1950 50  0001 L BNN "Check_prices"
	1    6700 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 654EFE52
P 5100 1750
AR Path="/654EFE52" Ref="R?"  Part="1" 
AR Path="/654E781B/654EFE52" Ref="R2"  Part="1" 
F 0 "R2" V 4893 1750 50  0000 C CNN
F 1 "402" V 4984 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5030 1750 50  0001 C CNN
F 3 "~" H 5100 1750 50  0001 C CNN
	1    5100 1750
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 654EFE59
P 5100 1850
AR Path="/654EFE59" Ref="R?"  Part="1" 
AR Path="/654E781B/654EFE59" Ref="R3"  Part="1" 
F 0 "R3" V 5200 1850 50  0000 C CNN
F 1 "402" V 5300 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5030 1850 50  0001 C CNN
F 3 "~" H 5100 1850 50  0001 C CNN
	1    5100 1850
	0    1    1    0   
$EndComp
Wire Wire Line
	4600 1750 4950 1750
Wire Wire Line
	4600 1850 4950 1850
Wire Wire Line
	7400 1850 7550 1850
Wire Wire Line
	5250 1750 5400 1750
Wire Wire Line
	5400 1750 5400 2000
Connection ~ 5400 1750
Wire Wire Line
	5400 1750 6000 1750
Wire Wire Line
	5250 1850 5550 1850
Wire Wire Line
	5550 1850 5550 1950
Wire Wire Line
	5550 1950 6000 1950
$Comp
L Device:R R?
U 1 1 654EFE6A
P 5400 2150
AR Path="/654EFE6A" Ref="R?"  Part="1" 
AR Path="/654E781B/654EFE6A" Ref="R6"  Part="1" 
F 0 "R6" H 5470 2196 50  0000 L CNN
F 1 "402" H 5470 2105 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5330 2150 50  0001 C CNN
F 3 "~" H 5400 2150 50  0001 C CNN
	1    5400 2150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 654EFE71
P 5400 2450
AR Path="/654EFE71" Ref="#PWR?"  Part="1" 
AR Path="/654E781B/654EFE71" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 5400 2200 50  0001 C CNN
F 1 "GND" H 5405 2277 50  0000 C CNN
F 2 "" H 5400 2450 50  0001 C CNN
F 3 "" H 5400 2450 50  0001 C CNN
	1    5400 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 2450 5400 2300
Connection ~ 5550 1850
Wire Wire Line
	7400 1750 7550 1750
$Comp
L Device:R R?
U 1 1 654EFE7A
P 6350 1100
AR Path="/654EFE7A" Ref="R?"  Part="1" 
AR Path="/654E781B/654EFE7A" Ref="R12"  Part="1" 
F 0 "R12" V 6143 1100 50  0000 C CNN
F 1 "402" V 6234 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 6280 1100 50  0001 C CNN
F 3 "~" H 6350 1100 50  0001 C CNN
	1    6350 1100
	0    1    1    0   
$EndComp
Wire Wire Line
	6500 1100 7550 1100
Wire Wire Line
	6200 1100 5550 1100
Wire Wire Line
	5550 1100 5550 1850
$Comp
L Device:R R?
U 1 1 654EFE84
P 6650 1200
AR Path="/654EFE84" Ref="R?"  Part="1" 
AR Path="/654E781B/654EFE84" Ref="R16"  Part="1" 
F 0 "R16" V 6443 1200 50  0000 C CNN
F 1 "25k" V 6534 1200 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 6580 1200 50  0001 C CNN
F 3 "~" H 6650 1200 50  0001 C CNN
	1    6650 1200
	0    1    1    0   
$EndComp
Wire Wire Line
	6800 1200 7550 1200
Wire Wire Line
	6500 1200 5700 1200
Wire Wire Line
	5700 1200 5700 1850
$Comp
L Device:R R?
U 1 1 654EFE8E
P 5700 2350
AR Path="/654EFE8E" Ref="R?"  Part="1" 
AR Path="/654E781B/654EFE8E" Ref="R8"  Part="1" 
F 0 "R8" H 5770 2396 50  0000 L CNN
F 1 "10k" H 5770 2305 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5630 2350 50  0001 C CNN
F 3 "~" H 5700 2350 50  0001 C CNN
	1    5700 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 1850 6000 1850
Wire Wire Line
	5700 1850 5700 2200
Connection ~ 5700 1850
$Comp
L power:GND #PWR?
U 1 1 654EFE98
P 5700 2650
AR Path="/654EFE98" Ref="#PWR?"  Part="1" 
AR Path="/654E781B/654EFE98" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 5700 2400 50  0001 C CNN
F 1 "GND" H 5705 2477 50  0000 C CNN
F 2 "" H 5700 2650 50  0001 C CNN
F 3 "" H 5700 2650 50  0001 C CNN
	1    5700 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2650 5700 2500
$Comp
L Device:R R?
U 1 1 654EFE9F
P 6000 2900
AR Path="/654EFE9F" Ref="R?"  Part="1" 
AR Path="/654E781B/654EFE9F" Ref="R10"  Part="1" 
F 0 "R10" H 5930 2854 50  0000 R CNN
F 1 "10k" H 5930 2945 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5930 2900 50  0001 C CNN
F 3 "~" H 6000 2900 50  0001 C CNN
	1    6000 2900
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 654EFEA6
P 6350 2600
AR Path="/654EFEA6" Ref="R?"  Part="1" 
AR Path="/654E781B/654EFEA6" Ref="R13"  Part="1" 
F 0 "R13" V 6450 2600 50  0000 C CNN
F 1 "25k" V 6550 2600 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 6280 2600 50  0001 C CNN
F 3 "~" H 6350 2600 50  0001 C CNN
	1    6350 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	6500 2600 7550 2600
$Comp
L Device:C C?
U 1 1 654EFEAE
P 3200 1750
AR Path="/654EFEAE" Ref="C?"  Part="1" 
AR Path="/654E781B/654EFEAE" Ref="C3"  Part="1" 
F 0 "C3" V 2948 1750 50  0000 C CNN
F 1 "0.1u" V 3039 1750 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 3238 1600 50  0001 C CNN
F 3 "~" H 3200 1750 50  0001 C CNN
	1    3200 1750
	0    1    1    0   
$EndComp
Wire Wire Line
	3350 1750 3500 1750
Wire Wire Line
	2900 1750 2900 1400
Wire Wire Line
	2900 1400 3950 1400
Wire Wire Line
	2900 1750 3050 1750
Connection ~ 3950 1400
Wire Wire Line
	7400 1550 7700 1550
Wire Wire Line
	7550 1750 7550 1200
Connection ~ 7550 1200
Wire Wire Line
	7550 1100 7550 1200
Wire Wire Line
	7400 2350 7700 2350
Wire Wire Line
	7550 1850 7550 2600
Connection ~ 7550 1850
Wire Wire Line
	7550 1850 7950 1850
Wire Wire Line
	7700 1550 7700 1400
Wire Wire Line
	7700 2350 7700 2500
Wire Wire Line
	6000 2050 6000 2600
Wire Wire Line
	6200 2600 6000 2600
Connection ~ 6000 2600
Wire Wire Line
	6000 2600 6000 2750
Wire Wire Line
	6000 3050 6000 3200
Wire Wire Line
	4600 2950 4750 2950
$Comp
L Device:C C?
U 1 1 654EFECA
P 4900 2950
AR Path="/654EFECA" Ref="C?"  Part="1" 
AR Path="/654E781B/654EFECA" Ref="C5"  Part="1" 
F 0 "C5" V 4648 2950 50  0000 C CNN
F 1 "C" V 4739 2950 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4938 2800 50  0001 C CNN
F 3 "~" H 4900 2950 50  0001 C CNN
	1    4900 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	5050 2950 5200 2950
Wire Wire Line
	4600 2750 5200 2750
Wire Wire Line
	5200 2750 5200 2950
Wire Wire Line
	5200 2950 5200 3100
Connection ~ 5200 2950
$Comp
L power:GND #PWR?
U 1 1 654EFED6
P 5200 3100
AR Path="/654EFED6" Ref="#PWR?"  Part="1" 
AR Path="/654E781B/654EFED6" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 5200 2850 50  0001 C CNN
F 1 "GND" H 5205 2927 50  0000 C CNN
F 2 "" H 5200 3100 50  0001 C CNN
F 3 "" H 5200 3100 50  0001 C CNN
	1    5200 3100
	1    0    0    -1  
$EndComp
Text GLabel 7700 1400 1    50   Input ~ 0
+5V
Text GLabel 7700 2500 3    50   Input ~ 0
-5V
Text GLabel 7950 1850 2    50   Input ~ 0
Signal_I
NoConn ~ 4600 2850
Text GLabel 6000 3200 3    50   Input ~ 0
Vr
Text GLabel 8800 1850 0    50   Input ~ 0
Signal_I
Wire Wire Line
	8800 1850 8950 1850
$Comp
L Connector:Conn_Coaxial J?
U 1 1 654EFEE3
P 9150 1850
AR Path="/654EFEE3" Ref="J?"  Part="1" 
AR Path="/654E781B/654EFEE3" Ref="J6"  Part="1" 
F 0 "J6" H 9249 1826 50  0000 L CNN
F 1 "Conn_Coaxial" H 9249 1735 50  0000 L CNN
F 2 "Connector_Coaxial:SMA_Amphenol_901-144_Vertical" H 9150 1850 50  0001 C CNN
F 3 " ~" H 9150 1850 50  0001 C CNN
	1    9150 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 2050 9150 2200
$Comp
L power:GND #PWR?
U 1 1 654EFEEB
P 9150 2200
AR Path="/654EFEEB" Ref="#PWR?"  Part="1" 
AR Path="/654E781B/654EFEEB" Ref="#PWR012"  Part="1" 
F 0 "#PWR012" H 9150 1950 50  0001 C CNN
F 1 "GND" H 9155 2027 50  0000 C CNN
F 2 "" H 9150 2200 50  0001 C CNN
F 3 "" H 9150 2200 50  0001 C CNN
	1    9150 2200
	1    0    0    -1  
$EndComp
$Comp
L MAX5190:max5190 U?
U 1 1 654EFEF8
P 3900 4200
AR Path="/654EFEF8" Ref="U?"  Part="1" 
AR Path="/654E781B/654EFEF8" Ref="U3"  Part="1" 
F 0 "U3" H 3700 4100 50  0000 C CNN
F 1 "max5190" H 4300 4100 50  0000 C CNN
F 2 "Package_SO:QSOP-24_3.9x8.7mm_P0.635mm" H 3900 4550 50  0001 C CNN
F 3 "" H 3900 4550 50  0001 C CNN
	1    3900 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 4950 3500 4950
Wire Wire Line
	2200 5050 3500 5050
Wire Wire Line
	2200 5150 3500 5150
Wire Wire Line
	2200 5250 3500 5250
Wire Wire Line
	2200 5450 3500 5450
Wire Wire Line
	3500 5550 2200 5550
Wire Wire Line
	2200 5650 3500 5650
Wire Wire Line
	2200 5350 3500 5350
Wire Wire Line
	3850 5850 3850 6000
Wire Wire Line
	4150 6000 4150 5850
Wire Wire Line
	3850 6000 3950 6000
Wire Wire Line
	4050 5850 4050 6000
Connection ~ 4050 6000
Wire Wire Line
	4050 6000 4150 6000
Wire Wire Line
	3950 5850 3950 6000
Connection ~ 3950 6000
Wire Wire Line
	3950 6000 4000 6000
Wire Wire Line
	4000 6000 4000 6150
Connection ~ 4000 6000
Wire Wire Line
	4000 6000 4050 6000
$Comp
L power:GND #PWR?
U 1 1 654EFF13
P 4000 6150
AR Path="/654EFF13" Ref="#PWR?"  Part="1" 
AR Path="/654E781B/654EFF13" Ref="#PWR05"  Part="1" 
F 0 "#PWR05" H 4000 5900 50  0001 C CNN
F 1 "GND" H 4005 5977 50  0000 C CNN
F 2 "" H 4000 6150 50  0001 C CNN
F 3 "" H 4000 6150 50  0001 C CNN
	1    4000 6150
	1    0    0    -1  
$EndComp
Text GLabel 4000 3950 1    50   Input ~ 0
+3V
Wire Wire Line
	4000 3950 4000 4100
Wire Wire Line
	4000 4100 3950 4100
Wire Wire Line
	3950 4100 3950 4250
Wire Wire Line
	4000 4100 4050 4100
Wire Wire Line
	4050 4100 4050 4250
Connection ~ 4000 4100
Wire Wire Line
	2550 4550 3500 4550
Wire Wire Line
	3500 4650 2550 4650
Wire Wire Line
	2550 4750 3500 4750
Wire Wire Line
	3500 4850 2550 4850
$Comp
L TLV3542IDR:TLV3542IDR U?
U 1 1 654EFF34
P 6700 4650
AR Path="/654EFF34" Ref="U?"  Part="1" 
AR Path="/654E781B/654EFF34" Ref="U5"  Part="1" 
F 0 "U5" H 6250 5200 50  0000 C CNN
F 1 "TLV3542IDR" H 7000 5200 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 6700 4650 50  0001 L BNN
F 3 "" H 6700 4650 50  0001 L BNN
F 4 "Texas Instruments" H 6700 4650 50  0001 L BNN "MF"
F 5 "\\nDual , 200MHz, RRIO, CMOS Operational Amplifier\\n" H 6700 4650 50  0001 L BNN "Description"
F 6 "SOIC-8 Texas Instruments" H 6700 4650 50  0001 L BNN "Package"
F 7 "None" H 6700 4650 50  0001 L BNN "Price"
F 8 "https://www.snapeda.com/parts/TLV3542IDR/Texas+Instruments/view-part/?ref=snap" H 6700 4650 50  0001 L BNN "SnapEDA_Link"
F 9 "TLV3542IDR" H 6700 4650 50  0001 L BNN "MP"
F 10 "https://www.snapeda.com/api/url_track_click_mouser/?unipart_id=802337&manufacturer=Texas Instruments&part_name=TLV3542IDR&search_term=None" H 6700 4650 50  0001 L BNN "Purchase-URL"
F 11 "In Stock" H 6700 4650 50  0001 L BNN "Availability"
F 12 "https://www.snapeda.com/parts/TLV3542IDR/Texas+Instruments/view-part/?ref=eda" H 6700 4650 50  0001 L BNN "Check_prices"
	1    6700 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 654EFF3B
P 5100 4450
AR Path="/654EFF3B" Ref="R?"  Part="1" 
AR Path="/654E781B/654EFF3B" Ref="R4"  Part="1" 
F 0 "R4" V 4893 4450 50  0000 C CNN
F 1 "402" V 4984 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5030 4450 50  0001 C CNN
F 3 "~" H 5100 4450 50  0001 C CNN
	1    5100 4450
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 654EFF42
P 5100 4550
AR Path="/654EFF42" Ref="R?"  Part="1" 
AR Path="/654E781B/654EFF42" Ref="R5"  Part="1" 
F 0 "R5" V 5200 4550 50  0000 C CNN
F 1 "402" V 5300 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5030 4550 50  0001 C CNN
F 3 "~" H 5100 4550 50  0001 C CNN
	1    5100 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	4600 4450 4950 4450
Wire Wire Line
	4600 4550 4950 4550
Wire Wire Line
	7400 4550 7550 4550
Wire Wire Line
	5250 4450 5400 4450
Wire Wire Line
	5400 4450 5400 4700
Connection ~ 5400 4450
Wire Wire Line
	5400 4450 6000 4450
Wire Wire Line
	5250 4550 5550 4550
Wire Wire Line
	5550 4550 5550 4650
Wire Wire Line
	5550 4650 6000 4650
$Comp
L Device:R R?
U 1 1 654EFF53
P 5400 4850
AR Path="/654EFF53" Ref="R?"  Part="1" 
AR Path="/654E781B/654EFF53" Ref="R7"  Part="1" 
F 0 "R7" H 5470 4896 50  0000 L CNN
F 1 "402" H 5470 4805 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5330 4850 50  0001 C CNN
F 3 "~" H 5400 4850 50  0001 C CNN
	1    5400 4850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 654EFF5A
P 5400 5150
AR Path="/654EFF5A" Ref="#PWR?"  Part="1" 
AR Path="/654E781B/654EFF5A" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 5400 4900 50  0001 C CNN
F 1 "GND" H 5405 4977 50  0000 C CNN
F 2 "" H 5400 5150 50  0001 C CNN
F 3 "" H 5400 5150 50  0001 C CNN
	1    5400 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 5150 5400 5000
Connection ~ 5550 4550
Wire Wire Line
	7400 4450 7550 4450
$Comp
L Device:R R?
U 1 1 654EFF63
P 6350 3800
AR Path="/654EFF63" Ref="R?"  Part="1" 
AR Path="/654E781B/654EFF63" Ref="R14"  Part="1" 
F 0 "R14" V 6143 3800 50  0000 C CNN
F 1 "402" V 6234 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 6280 3800 50  0001 C CNN
F 3 "~" H 6350 3800 50  0001 C CNN
	1    6350 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	6500 3800 7550 3800
Wire Wire Line
	6200 3800 5550 3800
Wire Wire Line
	5550 3800 5550 4550
$Comp
L Device:R R?
U 1 1 654EFF6D
P 6650 3900
AR Path="/654EFF6D" Ref="R?"  Part="1" 
AR Path="/654E781B/654EFF6D" Ref="R17"  Part="1" 
F 0 "R17" V 6443 3900 50  0000 C CNN
F 1 "25k" V 6534 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 6580 3900 50  0001 C CNN
F 3 "~" H 6650 3900 50  0001 C CNN
	1    6650 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	6800 3900 7550 3900
Wire Wire Line
	6500 3900 5700 3900
Wire Wire Line
	5700 3900 5700 4550
$Comp
L Device:R R?
U 1 1 654EFF77
P 5700 5050
AR Path="/654EFF77" Ref="R?"  Part="1" 
AR Path="/654E781B/654EFF77" Ref="R9"  Part="1" 
F 0 "R9" H 5770 5096 50  0000 L CNN
F 1 "10k" H 5770 5005 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5630 5050 50  0001 C CNN
F 3 "~" H 5700 5050 50  0001 C CNN
	1    5700 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 4550 6000 4550
Wire Wire Line
	5700 4550 5700 4900
Connection ~ 5700 4550
$Comp
L power:GND #PWR?
U 1 1 654EFF81
P 5700 5350
AR Path="/654EFF81" Ref="#PWR?"  Part="1" 
AR Path="/654E781B/654EFF81" Ref="#PWR011"  Part="1" 
F 0 "#PWR011" H 5700 5100 50  0001 C CNN
F 1 "GND" H 5705 5177 50  0000 C CNN
F 2 "" H 5700 5350 50  0001 C CNN
F 3 "" H 5700 5350 50  0001 C CNN
	1    5700 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 5350 5700 5200
$Comp
L Device:R R?
U 1 1 654EFF88
P 6000 5600
AR Path="/654EFF88" Ref="R?"  Part="1" 
AR Path="/654E781B/654EFF88" Ref="R11"  Part="1" 
F 0 "R11" H 5930 5554 50  0000 R CNN
F 1 "10k" H 5930 5645 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5930 5600 50  0001 C CNN
F 3 "~" H 6000 5600 50  0001 C CNN
	1    6000 5600
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 654EFF8F
P 6350 5300
AR Path="/654EFF8F" Ref="R?"  Part="1" 
AR Path="/654E781B/654EFF8F" Ref="R15"  Part="1" 
F 0 "R15" V 6450 5300 50  0000 C CNN
F 1 "25k" V 6550 5300 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 6280 5300 50  0001 C CNN
F 3 "~" H 6350 5300 50  0001 C CNN
	1    6350 5300
	0    1    1    0   
$EndComp
Wire Wire Line
	6500 5300 7550 5300
$Comp
L Device:C C?
U 1 1 654EFF97
P 3200 4450
AR Path="/654EFF97" Ref="C?"  Part="1" 
AR Path="/654E781B/654EFF97" Ref="C4"  Part="1" 
F 0 "C4" V 2948 4450 50  0000 C CNN
F 1 "0.1u" V 3039 4450 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 3238 4300 50  0001 C CNN
F 3 "~" H 3200 4450 50  0001 C CNN
	1    3200 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	3350 4450 3500 4450
Wire Wire Line
	2900 4450 2900 4100
Wire Wire Line
	2900 4100 3950 4100
Wire Wire Line
	2900 4450 3050 4450
Connection ~ 3950 4100
Wire Wire Line
	7400 4250 7700 4250
Wire Wire Line
	7550 4450 7550 3900
Connection ~ 7550 3900
Wire Wire Line
	7550 3800 7550 3900
Wire Wire Line
	7400 5050 7700 5050
Wire Wire Line
	7550 4550 7550 5300
Connection ~ 7550 4550
Wire Wire Line
	7550 4550 7950 4550
Wire Wire Line
	7700 4250 7700 4100
Wire Wire Line
	7700 5050 7700 5200
Wire Wire Line
	6000 4750 6000 5300
Wire Wire Line
	6200 5300 6000 5300
Connection ~ 6000 5300
Wire Wire Line
	6000 5300 6000 5450
Wire Wire Line
	6000 5750 6000 5900
Wire Wire Line
	4600 5650 4750 5650
$Comp
L Device:C C?
U 1 1 654EFFB3
P 4900 5650
AR Path="/654EFFB3" Ref="C?"  Part="1" 
AR Path="/654E781B/654EFFB3" Ref="C6"  Part="1" 
F 0 "C6" V 4648 5650 50  0000 C CNN
F 1 "C" V 4739 5650 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4938 5500 50  0001 C CNN
F 3 "~" H 4900 5650 50  0001 C CNN
	1    4900 5650
	0    1    1    0   
$EndComp
Wire Wire Line
	5050 5650 5200 5650
Wire Wire Line
	4600 5450 5200 5450
Wire Wire Line
	5200 5450 5200 5650
Wire Wire Line
	5200 5650 5200 5800
Connection ~ 5200 5650
$Comp
L power:GND #PWR?
U 1 1 654EFFBF
P 5200 5800
AR Path="/654EFFBF" Ref="#PWR?"  Part="1" 
AR Path="/654E781B/654EFFBF" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 5200 5550 50  0001 C CNN
F 1 "GND" H 5205 5627 50  0000 C CNN
F 2 "" H 5200 5800 50  0001 C CNN
F 3 "" H 5200 5800 50  0001 C CNN
	1    5200 5800
	1    0    0    -1  
$EndComp
Text GLabel 7700 4100 1    50   Input ~ 0
+5V
Text GLabel 7700 5200 3    50   Input ~ 0
-5V
Text GLabel 7950 4550 2    50   Input ~ 0
Signal_Q
NoConn ~ 4600 5550
Text GLabel 6000 5900 3    50   Input ~ 0
Vr
Text GLabel 8800 4550 0    50   Input ~ 0
Signal_Q
Wire Wire Line
	8800 4550 8950 4550
$Comp
L Connector:Conn_Coaxial J?
U 1 1 654EFFCC
P 9150 4550
AR Path="/654EFFCC" Ref="J?"  Part="1" 
AR Path="/654E781B/654EFFCC" Ref="J7"  Part="1" 
F 0 "J7" H 9249 4526 50  0000 L CNN
F 1 "Conn_Coaxial" H 9249 4435 50  0000 L CNN
F 2 "Connector_Coaxial:SMA_Amphenol_901-144_Vertical" H 9150 4550 50  0001 C CNN
F 3 " ~" H 9150 4550 50  0001 C CNN
	1    9150 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 4750 9150 4900
$Comp
L power:GND #PWR?
U 1 1 654EFFD4
P 9150 4900
AR Path="/654EFFD4" Ref="#PWR?"  Part="1" 
AR Path="/654E781B/654EFFD4" Ref="#PWR013"  Part="1" 
F 0 "#PWR013" H 9150 4650 50  0001 C CNN
F 1 "GND" H 9155 4727 50  0000 C CNN
F 2 "" H 9150 4900 50  0001 C CNN
F 3 "" H 9150 4900 50  0001 C CNN
	1    9150 4900
	1    0    0    -1  
$EndComp
Text GLabel 2200 2250 0    50   BiDi ~ 0
1
Text GLabel 2200 2450 0    50   BiDi ~ 0
3
Text GLabel 2200 2650 0    50   BiDi ~ 0
5
Text GLabel 2200 2850 0    50   BiDi ~ 0
9
Text GLabel 2200 2350 0    50   BiDi ~ 0
2
Text GLabel 2200 2550 0    50   BiDi ~ 0
4
Text GLabel 2200 2750 0    50   BiDi ~ 0
6
Text GLabel 2200 2950 0    50   BiDi ~ 0
10
Text GLabel 2550 1850 0    50   BiDi ~ 0
11
Text GLabel 2550 2050 0    50   BiDi ~ 0
13
Text GLabel 2550 1950 0    50   BiDi ~ 0
12
Text GLabel 2550 2150 0    50   BiDi ~ 0
14
Text GLabel 2200 4950 0    50   BiDi ~ 0
19
Text GLabel 2200 5050 0    50   BiDi ~ 0
21
Text GLabel 2200 5150 0    50   BiDi ~ 0
23
Text GLabel 2200 5250 0    50   BiDi ~ 0
25
Text GLabel 2200 5350 0    50   BiDi ~ 0
20
Text GLabel 2200 5450 0    50   BiDi ~ 0
22
Text GLabel 2200 5550 0    50   BiDi ~ 0
24
Text GLabel 2200 5650 0    50   BiDi ~ 0
26
Text GLabel 2550 4550 0    50   BiDi ~ 0
27
Text GLabel 2550 4650 0    50   BiDi ~ 0
28
Text GLabel 2550 4750 0    50   BiDi ~ 0
32
Text GLabel 2550 4850 0    50   BiDi ~ 0
33
Text Label 4850 1750 2    50   ~ 0
out1_P
Text Label 4850 1850 2    50   ~ 0
out1_N
Text Label 5450 1750 2    50   ~ 0
r1+
Text Label 5450 1850 2    50   ~ 0
r1-
$EndSCHEMATC
