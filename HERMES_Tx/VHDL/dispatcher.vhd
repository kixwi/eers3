-- 	DISPATCHER.VHD
-- 	VHDL HERMES TX
-- 	2023 IUT GEII BRIVE
-- 	AUTHORS : 
--		JULIEN LAFFEZ
-- 		KILLIAN LOUISE
-- 		JOEL ANDRIEU
--	LICENSE : MIT
-- 	REV : 20240905

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY DISPATCHER IS
	PORT(
	I_MUX_MODE: IN STD_LOGIC; -- MUX MODE
	I_DATA: IN STD_LOGIC_VECTOR(11 DOWNTO 0); -- MUX DATA
	I_SEL_PUSH: IN STD_LOGIC; -- MODE SELECT
	I_DSP_RST: IN STD_LOGIC; -- RESET DISPATCHER
	I_LATCH, I_CLK: IN STD_LOGIC; -- EXTERNAL INPUTS
	O_TX: OUT STD_LOGIC; -- TRANSMISSION SIGNAL OUTPUT
	O_LED_MODE : OUT STD_LOGIC_VECTOR(1 DOWNTO 0); -- MODULATION MODE OUTPUTS
	O_CH_I_DATA, O_CH_Q_DATA: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	--O_LFSR_RST, 
	O_PLL_RST, O_PWR_I_MODE, O_PWR_Q_MODE : OUT STD_LOGIC); 
END ENTITY;

ARCHITECTURE DESC OF DISPATCHER IS
	SIGNAL INT_TX: STD_LOGIC; -- TRANSMISSION BIT
	SIGNAL DATA_KEEP: STD_LOGIC_VECTOR(11 DOWNTO 0);
	SIGNAL TAB_ASK: STD_LOGIC; -- TABLEAU POUR LE BIT DECOUPE ASK
	SIGNAL TAB_QPSK: STD_LOGIC_VECTOR(1 DOWNTO 0) := "00"; -- CONSTELLATION COORDINATES FOR QPSK
	SIGNAL TAB_8PSK: STD_LOGIC_VECTOR(2 DOWNTO 0); -- CONSTELLATION COORDINATES FOR 8PSK
	SIGNAL INT_MODE_SELECT: STD_LOGIC_VECTOR(1 DOWNTO 0) := "00"; -- MODE SELECTOR
	SIGNAL INT_CH_I_DATA, INT_CH_Q_DATA: STD_LOGIC_VECTOR(7 DOWNTO 0) := "00000000";
	SIGNAL ASK_CPT: INTEGER RANGE 0 TO 12; -- FOR ASK ONLY
	SIGNAL QPSK_CPT: INTEGER RANGE 0 TO 12; -- FOR QPSK ONLY
	SIGNAL PSK8_CPT: INTEGER RANGE 0 TO 12; -- FOR 8-PSK ONLY
	
	signal test : STD_LOGIC;
	
	constant niveauPositif1V : 		STD_LOGIC_VECTOR(7 DOWNTO 0) := "11111111";
	constant niveauPositif747mV : 	STD_LOGIC_VECTOR(7 DOWNTO 0) := "11011011";
	constant niveau0V : 			STD_LOGIC_VECTOR(7 DOWNTO 0) := "10000000";
	constant niveauNegatif747mV : 	STD_LOGIC_VECTOR(7 DOWNTO 0) := "00100110";
	constant niveauNegatif1V : 		STD_LOGIC_VECTOR(7 DOWNTO 0) := "00000000";

BEGIN

	-- 
	
	
	-- MODE SELECTOR
	INT_MODE_SELECTOR: PROCESS (I_DSP_RST, I_SEL_PUSH)
	BEGIN
		IF (I_DSP_RST='1') THEN 
			INT_MODE_SELECT <= "00";
		ELSIF (RISING_EDGE(I_SEL_PUSH)) THEN
			IF (INT_MODE_SELECT="11") THEN INT_MODE_SELECT <= "00";
			ELSE
				INT_MODE_SELECT <= INT_MODE_SELECT+1;
			END IF;
		END IF;
	END PROCESS INT_MODE_SELECTOR;
	
	O_LED_MODE <= INT_MODE_SELECT;
				
	-- O_PWR_I_MODE <= "10" WHEN (INT_MODE_SELECT="00") ELSE "01";
	-- O_PWR_Q_MODE <= "10" WHEN (INT_MODE_SELECT="00" OR INT_MODE_SELECT="01") ELSE "01";
	O_PWR_I_MODE <= '0' WHEN (INT_MODE_SELECT="00") ELSE '1'; 
	O_PWR_Q_MODE <= '0' WHEN (INT_MODE_SELECT="00" OR INT_MODE_SELECT="01") ELSE '1';
	
	--O_LFSR_RST <= '1' WHEN (INT_MODE_SELECT="00") ELSE '0';
	O_PLL_RST <= '0' WHEN (INT_MODE_SELECT="00") ELSE '1';
	
	-- DATA LATCH
	-- DATA_LATCH: PROCESS (I_CLK)
	-- BEGIN
		-- IF (I_CLK='1') THEN
			-- IF (I_MUX_MODE='0') THEN
				-- DATA_KEEP <= I_MUX_DATA;
				-- INT_TX <= '1';
			-- ELSE
				-- IF (I_LATCH='0') THEN
					-- DATA_KEEP <= DATA_KEEP;
					-- INT_TX <= '0';
				-- ELSE
					-- DATA_KEEP <= I_MUX_DATA;
					-- INT_TX <= '1';
				-- END IF;
			-- END IF;
		-- END IF;
	-- END PROCESS DATA_LATCH;
	
	-- O_TX <= INT_TX;
	
	-- Decoupage de la trame pour ASK
	TAB_ASK <= I_DATA(11 - ASK_CPT) WHEN (RISING_EDGE(I_CLK) AND INT_MODE_SELECT=1) ELSE TAB_ASK;
	
	-- Calcul des paires et trinomes de bits pour le QPSK et le 8PSK
	TAB_QPSK <= I_DATA(11 - QPSK_CPT) & I_DATA(10 - QPSK_CPT) when (rising_edge(I_CLK) and INT_MODE_SELECT=2) else TAB_QPSK;
	TAB_8PSK <= I_DATA(11 - PSK8_CPT) & I_DATA(10 - PSK8_CPT) & I_DATA(9 - PSK8_CPT) when (rising_edge(I_CLK) and INT_MODE_SELECT=3) else TAB_8PSK;

	-- Conversion DATA vers signaux de commande des DAC en fonction du MODE de modulation souhaite
	-- Partie voie I
	INT_CH_I_DATA <="11111111" 			when (TAB_ASK = '1' AND INT_MODE_SELECT=1) else
    				"10000000" 			when (TAB_ASK = '0' AND INT_MODE_SELECT=1) else
                    
                    niveauPositif1V 	when ((TAB_QPSK = "11" or TAB_QPSK = "10") and INT_MODE_SELECT=2) else
    				niveauNegatif1V	 	when ((TAB_QPSK = "00" or TAB_QPSK = "01") and INT_MODE_SELECT=2) else
                    
					niveauPositif1V 	when (TAB_8PSK = "111" and INT_MODE_SELECT=3) ELSE
					niveauPositif747mV 	when ((TAB_8PSK = "110" or TAB_8PSK = "101") and INT_MODE_SELECT=3) ELSE
					niveau0V 			when ((TAB_8PSK = "010" or TAB_8PSK = "100") and INT_MODE_SELECT=3) ELSE
					niveauNegatif747mV 	when ((TAB_8PSK = "011" or TAB_8PSK = "000") and INT_MODE_SELECT=3) ELSE
					niveauNegatif1V 	when (TAB_8PSK = "001" and INT_MODE_SELECT=3);

	-- Partie voie Q
	INT_CH_Q_DATA <=niveau0V			when INT_MODE_SELECT=1 else
	
                   	niveauPositif1V 	when ((TAB_QPSK = "01" or TAB_QPSK = "11") and INT_MODE_SELECT=2) else
                    niveauNegatif1V 	when ((TAB_QPSK = "00" or TAB_QPSK = "10") and INT_MODE_SELECT=2) else
					
					niveauPositif1V 	when (TAB_8PSK = "010" and INT_MODE_SELECT=3) ELSE
					niveauPositif747mV 	when ((TAB_8PSK = "110" or TAB_8PSK = "011") and INT_MODE_SELECT=3) ELSE
					niveau0V 			when ((TAB_8PSK = "111" or TAB_8PSK = "001") and INT_MODE_SELECT=3) ELSE
					niveauNegatif747mV 	when ((TAB_8PSK = "101" or TAB_8PSK = "000") and INT_MODE_SELECT=3) ELSE
					niveauNegatif1V 	when (TAB_8PSK = "100" and INT_MODE_SELECT=3);

	-- Increment des compteurs
	ASK_CPT <= 0 when ASK_CPT = 11 and (rising_edge(I_CLK) and INT_MODE_SELECT=1) else
            ASK_CPT + 1 when (rising_edge(I_CLK) and INT_MODE_SELECT=1) else
            ASK_CPT;
			
	QPSK_CPT <= 0 when QPSK_CPT = 10 and (rising_edge(I_CLK) and INT_MODE_SELECT=2) else
            QPSK_CPT + 2 when (rising_edge(I_CLK) and INT_MODE_SELECT=2) else
            QPSK_CPT;
			
	PSK8_CPT <= 0 when PSK8_CPT = 9 and (RISING_EDGE(I_CLK) and INT_MODE_SELECT=3) ELSE
				PSK8_CPT + 3 when (RISING_EDGE(I_CLK) and INT_MODE_SELECT=3) ELSE
				PSK8_CPT;
	
	O_CH_I_DATA <= INT_CH_I_DATA;
	O_CH_Q_DATA <= INT_CH_Q_DATA;
END DESC;