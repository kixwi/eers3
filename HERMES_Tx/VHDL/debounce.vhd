-- VHDL HERMES TX
-- 2023 IUT GEII BRIVE
-- AUTHORS : 
--		JULIEN LAFFEZ
-- 		KILLIAN LOUISE
-- 		JOEL ANDRIEU
--	DEBOUNCE.VHD

LIBRARY IEEE;

USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY DEBOUNCE IS
	PORT (
	I_CLK_20MHz: IN STD_LOGIC; -- CLOCK 1 MHz
	I_SIGNAL_UNBOUNCED: IN STD_LOGIC; -- SWITCH MODE SELECT
	I_DB_RST: IN STD_LOGIC; -- RESET ANTIREBOND
	O_PULSE: OUT STD_LOGIC); -- MODE SELECT
END ENTITY;

ARCHITECTURE DESC OF DEBOUNCE IS
SIGNAL DB_CPT: INTEGER RANGE 0 TO 5000 := 0;
TYPE STATE_TYPE IS (E0,E1);
SIGNAL ETAT_FUTUR : STATE_TYPE := E0;


BEGIN

Detecteur : PROCESS(I_CLK_20MHz, I_DB_RST, I_SIGNAL_UNBOUNCED)
    BEGIN
    IF(I_DB_RST='1') THEN 
        DB_CPT <= 0;
        ETAT_FUTUR <= E0;
    END IF;
        
    case ETAT_FUTUR IS
        WHEN E0 =>
            IF(I_SIGNAL_UNBOUNCED='1') THEN
                ETAT_FUTUR <= E1;
                DB_CPT <= 1;
            END IF;
        
        WHEN E1 =>
            IF(rising_edge(I_CLK_20MHz)) THEN
                IF(DB_CPT < 5000) THEN
                    DB_CPT <= DB_CPT + 1;
                else
                    IF(I_SIGNAL_UNBOUNCED='1') THEN
                        DB_CPT <= 10;
                    ELSE
                        ETAT_FUTUR <= E0;
                    END IF;
                END IF;
            END IF;
    END CASE;

    if(rising_edge(I_CLK_20MHz)) THEN
        if((DB_CPT > 0) AND(DB_CPT < 10)) THEN
            O_PULSE <= '1';
        else 
            O_PULSE <= '0';
        END IF;
    END IF;
END PROCESS;
END DESC;