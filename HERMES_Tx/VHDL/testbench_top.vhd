LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY tb_TOP IS
END tb_TOP;

ARCHITECTURE behavior OF tb_TOP IS

    -- Composant à tester (Unit Under Test, UUT)
    COMPONENT TOP
    PORT(
        I_DIP : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
        I_MODE_SW : IN  STD_LOGIC;
        I_LATCH : IN  STD_LOGIC;
        I_HERMES_RESET : IN  STD_LOGIC;
        O_PD_I : OUT  STD_LOGIC;
        O_DACEN_I : OUT  STD_LOGIC;
        O_CS_I : OUT  STD_LOGIC;
        O_CLK_I : OUT  STD_LOGIC;
        O_PD_Q : OUT  STD_LOGIC;
        O_DACEN_Q : OUT  STD_LOGIC;
        O_CS_Q : OUT  STD_LOGIC;
        O_CLK_Q : OUT  STD_LOGIC;
        O_DATA_I : OUT  STD_LOGIC_VECTOR(7 DOWNTO 0);
        O_DATA_Q : OUT  STD_LOGIC_VECTOR(7 DOWNTO 0);
        O_SCK : OUT  STD_LOGIC;
        O_PLL_DATA : OUT  STD_LOGIC;
        O_STROBE : OUT  STD_LOGIC;
        O_LED_PWR : OUT  STD_LOGIC;
        O_LED_I : OUT  STD_LOGIC;
        O_LED_Q : OUT  STD_LOGIC;
        O_LED_ASK : OUT  STD_LOGIC;
        O_LED_QPSK : OUT  STD_LOGIC;
        O_LED_8PSK : OUT  STD_LOGIC;
        O_LED_TX : OUT  STD_LOGIC
    );
    END COMPONENT;

    -- Déclaration des signaux pour connecter au UUT
    SIGNAL I_DIP : STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => '0');
    SIGNAL I_MODE_SW : STD_LOGIC := '0';
    SIGNAL I_LATCH : STD_LOGIC := '0';
    SIGNAL I_HERMES_RESET : STD_LOGIC := '0';
    SIGNAL O_PD_I : STD_LOGIC;
    SIGNAL O_DACEN_I : STD_LOGIC;
    SIGNAL O_CS_I : STD_LOGIC;
    SIGNAL O_CLK_I : STD_LOGIC;
    SIGNAL O_PD_Q : STD_LOGIC;
    SIGNAL O_DACEN_Q : STD_LOGIC;
    SIGNAL O_CS_Q : STD_LOGIC;
    SIGNAL O_CLK_Q : STD_LOGIC;
    SIGNAL O_DATA_I : STD_LOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL O_DATA_Q : STD_LOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL O_SCK : STD_LOGIC;
    SIGNAL O_PLL_DATA : STD_LOGIC;
    SIGNAL O_STROBE : STD_LOGIC;
    SIGNAL O_LED_PWR : STD_LOGIC;
    SIGNAL O_LED_I : STD_LOGIC;
    SIGNAL O_LED_Q : STD_LOGIC;
    SIGNAL O_LED_ASK : STD_LOGIC;
    SIGNAL O_LED_QPSK : STD_LOGIC;
    SIGNAL O_LED_8PSK : STD_LOGIC;
    SIGNAL O_LED_TX : STD_LOGIC;

BEGIN

    -- Instanciation du UUT
    uut: TOP PORT MAP (
        I_DIP => I_DIP,
        I_MODE_SW => I_MODE_SW,
        I_LATCH => I_LATCH,
        I_HERMES_RESET => I_HERMES_RESET,
        O_PD_I => O_PD_I,
        O_DACEN_I => O_DACEN_I,
        O_CS_I => O_CS_I,
        O_CLK_I => O_CLK_I,
        O_PD_Q => O_PD_Q,
        O_DACEN_Q => O_DACEN_Q,
        O_CS_Q => O_CS_Q,
        O_CLK_Q => O_CLK_Q,
        O_DATA_I => O_DATA_I,
        O_DATA_Q => O_DATA_Q,
        O_SCK => O_SCK,
        O_PLL_DATA => O_PLL_DATA,
        O_STROBE => O_STROBE,
        O_LED_PWR => O_LED_PWR,
        O_LED_I => O_LED_I,
        O_LED_Q => O_LED_Q,
        O_LED_ASK => O_LED_ASK,
        O_LED_QPSK => O_LED_QPSK,
        O_LED_8PSK => O_LED_8PSK,
        O_LED_TX => O_LED_TX
    );

    -- Stimuli process
    stim_proc: PROCESS
    BEGIN
        -- Initial reset
        I_HERMES_RESET <= '1';
        WAIT FOR 50 ns;
        I_HERMES_RESET <= '0';
        
        -- Test 1: Mode DIP
        I_DIP <= "10001101";
        I_MODE_SW <= '0';
        
        WAIT FOR 50 ns;

        
        I_MODE_SW <= '1';
        WAIT FOR 50 ns;
		I_MODE_SW <= '0';
        
        WAIT FOR 50 NS;
        I_MODE_SW <= '1';
        WAIT FOR 50 ns;
		I_MODE_SW <= '0';
		
		WAIT FOR 12 US;
		I_MODE_SW <= '1';
		WAIT FOR 50 NS;
		I_MODE_SW <= '0';

        -- Fin de la simulation
        WAIT;
    END PROCESS;

END behavior;
