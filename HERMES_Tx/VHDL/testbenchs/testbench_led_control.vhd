-- 	TESTBENCH.VHD
-- 	VHDL HERMES TX
-- 	2023 IUT GEII BRIVE
-- 	AUTHORS : 
--		JULIEN LAFFEZ
-- 		KILLIAN LOUISE
-- 		JOEL ANDRIEU
--	LICENSE : MIT
-- 	REV : 20240825

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY TB_LED_CONTROL IS
END ENTITY;

ARCHITECTURE SIM OF TB_LED_CONTROL IS

    -- Déclarations des signaux internes pour le test
    SIGNAL I_TX       : STD_LOGIC := '0';
    SIGNAL I_CLK_1    : STD_LOGIC := '0';
    SIGNAL I_LED_MODE : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00";
    SIGNAL O_LED_PWR  : STD_LOGIC;
    SIGNAL O_CH_I_LED : STD_LOGIC;
    SIGNAL O_CH_Q_LED : STD_LOGIC;
    SIGNAL O_ASK_LED  : STD_LOGIC;
    SIGNAL O_QPSK_LED : STD_LOGIC;
    SIGNAL O_8PSK_LED : STD_LOGIC;
    SIGNAL O_TX_LED   : STD_LOGIC;

    -- Instance de l'entité à tester
    COMPONENT LED_CONTROL
    PORT(
        I_TX       : IN  STD_LOGIC;
        I_CLK_1    : IN  STD_LOGIC;
        I_LED_MODE : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
        O_LED_PWR  : OUT STD_LOGIC;
        O_CH_I_LED : OUT STD_LOGIC;
        O_CH_Q_LED : OUT STD_LOGIC;
        O_ASK_LED  : OUT STD_LOGIC;
        O_QPSK_LED : OUT STD_LOGIC;
        O_8PSK_LED : OUT STD_LOGIC;
        O_TX_LED   : OUT STD_LOGIC
    );
    END COMPONENT;

BEGIN

    -- Liaison des signaux à l'instance
    UUT: LED_CONTROL
    PORT MAP(
        I_TX       => I_TX,
        I_CLK_1    => I_CLK_1,
        I_LED_MODE => I_LED_MODE,
        O_LED_PWR  => O_LED_PWR,
        O_CH_I_LED => O_CH_I_LED,
        O_CH_Q_LED => O_CH_Q_LED,
        O_ASK_LED  => O_ASK_LED,
        O_QPSK_LED => O_QPSK_LED,
        O_8PSK_LED => O_8PSK_LED,
        O_TX_LED   => O_TX_LED
    );

    -- Génération du signal d'horloge
    I_CLK_1_PROCESS: PROCESS
    BEGIN
        WHILE TRUE LOOP
            I_CLK_1 <= '0';
            WAIT FOR 500 ms;
            I_CLK_1 <= '1';
            WAIT FOR 500 ms;
        END LOOP;
    END PROCESS;

    -- Processus de test
    TEST_PROCESS: PROCESS
    BEGIN
        -- Cas 1 : Mode = 00, I_TX = 0, LED TX doit être éteinte
        I_LED_MODE <= "00";
        I_TX <= '0';
        WAIT FOR 1 sec;

        -- Cas 2 : Mode = 00, I_TX = 1, LED TX doit clignoter
        I_TX <= '1';
        WAIT FOR 2 sec;

        -- Cas 3 : Mode = 01, LED ASK doit être allumée
        I_LED_MODE <= "01";
        WAIT FOR 1 sec;

        -- Cas 4 : Mode = 10, LED QPSK doit être allumée
        I_LED_MODE <= "10";
        WAIT FOR 1 sec;

        -- Cas 5 : Mode = 11, LED 8PSK doit être allumée
        I_LED_MODE <= "11";
        WAIT FOR 1 sec;

        -- Fin du test
        WAIT;
    END PROCESS;

END ARCHITECTURE;