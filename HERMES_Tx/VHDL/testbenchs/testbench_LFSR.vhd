-- 	TESTBENCH.VHD
-- 	VHDL HERMES TX
-- 	2023 IUT GEII BRIVE
-- 	AUTHORS : 
--		JULIEN LAFFEZ
-- 		KILLIAN LOUISE
-- 		JOEL ANDRIEU
--	LICENSE : MIT
-- 	REV : 20240413

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY TB_LFSR IS
END TB_LFSR;

ARCHITECTURE BEHAVIOR OF TB_LFSR IS

  -- COMPONENT DECLARATION FOR LFSR
  COMPONENT LFSR IS
    PORT (
      I_CLK_1M : IN STD_LOGIC;
      I_LFSR_RST : IN STD_LOGIC;
      O_LFSR : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
    );
  END COMPONENT;

  -- SIGNALS
  SIGNAL CLK_1M : STD_LOGIC := '0';
  SIGNAL LFSR_RST : STD_LOGIC := '1';
  SIGNAL LFSR_OUT : STD_LOGIC_VECTOR(7 DOWNTO 0);

  -- EXPECTED OUTPUT SEQUENCE (REPLACE WITH ACTUAL SEQUENCE IF NEEDED)
  CONSTANT EXPECTED_OUTPUT : STD_LOGIC_VECTOR(7 DOWNTO 0) := "01100010";

BEGIN

  -- INSTANTIATE LFSR ENTITY
  UUT : LFSR PORT MAP (
    I_CLK_1M => CLK_1M,
    I_LFSR_RST => LFSR_RST,
    O_LFSR => LFSR_OUT
  );

	CLK_1M <= NOT CLK_1M AFTER 1 NS;
    
    PROCESS
    BEGIN
    	LFSR_RST <= '1';
        WAIT FOR 4 NS;
        LFSR_RST <= '0';
        WAIT FOR 10000 NS;
    END PROCESS;
    

END BEHAVIOR;