-- 	TESTBENCH.VHD
-- 	VHDL HERMES TX
-- 	2023 IUT GEII BRIVE
-- 	AUTHORS : 
--		JULIEN LAFFEZ
-- 		KILLIAN LOUISE
-- 		JOEL ANDRIEU
--	LICENSE : MIT
-- 	REV : 20240423

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Include your DISPATCHER entity here
ENTITY dispatcher_tb IS 
  -- Insert ports from DISPATCHER entity here
END ENTITY;

ARCHITECTURE behavior OF dispatcher_tb IS

  -- Component declaration for DISPATCHER
  COMPONENT dispatcher
  PORT (
    I_MUX_MODE: IN STD_LOGIC; -- MUX MODE
	I_MUX_DATA: IN STD_LOGIC_VECTOR(7 DOWNTO 0); -- MUX DATA
	I_SEL_PUSH: IN STD_LOGIC; -- MODE SELECT
	I_DSP_RST: IN STD_LOGIC; -- RESET DISPATCHER
	I_LATCH, I_CLK_1M: IN STD_LOGIC; -- EXTERNAL INPUTS
	O_TX: OUT STD_LOGIC; -- TRANSMISSION SIGNAL OUTPUT
	O_LED_MODE : OUT STD_LOGIC_VECTOR(1 DOWNTO 0); -- MODULATION MODE OUTPUTS
	O_CH_I_DATA, O_CH_Q_DATA: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	O_LFSR_RST, O_PLL_RST, O_PWR_I_MODE, O_PWR_Q_MODE : OUT STD_LOGIC); 
  
  END COMPONENT;

  -- Signals for test stimulus
  SIGNAL clk_1m : STD_LOGIC := '0';
  SIGNAL sel_push : STD_LOGIC := '0';
  SIGNAL dsp_rst : STD_LOGIC := '1';
  SIGNAL mux_mode : STD_LOGIC := '0';
  SIGNAL mux_data : STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => '0');
  signal led_mode : STD_LOGIC_VECTOR(1 DOWNTO 0);
  signal pwr_i_mode : STD_LOGIC;
  signal pwr_q_mode : STD_LOGIC;
  SIGNAL ch_i_data : STD_LOGIC_VECTOR(7 DOWNTO 0);
  signal ch_q_data : STD_LOGIC_VECTOR(7 DOWNTO 0);
  SIGNAL lfsr_rst : STD_LOGIC;
  SIGNAL pll_rst : STD_LOGIC;
  
  SIGNAL stop : boolean := false;

BEGIN
  -- DUT (Device Under Test) instantiation
  UUT : dispatcher PORT MAP (
    I_MUX_MODE => mux_mode,
    I_MUX_DATA => mux_data,
    I_SEL_PUSH => sel_push,
    I_DSP_RST => dsp_rst,
    I_LATCH => '0', -- Assume LATCH is not used in this test
    I_CLK_1M => clk_1m,
    O_TX => OPEN, -- We don't need to monitor TX for this test
    O_LED_MODE => led_mode,
    O_PWR_I_MODE => pwr_i_mode,
    O_PWR_Q_MODE => pwr_q_mode,
    O_CH_I_DATA => ch_i_data,
    O_CH_Q_DATA => ch_q_data,
    O_LFSR_RST => lfsr_rst,
    O_PLL_RST => pll_rst
  );

  -- Process to generate clock signal
  CLK_GEN : PROCESS
  BEGIN
	while not stop loop
		CLK_1M <= NOT CLK_1M;
		WAIT FOR 0.5 us;
	end loop;
  END PROCESS;

-- Insert processes to generate test stimulus (sel_push, mux_mode, mux_data) here

stimulus : process 

BEGIN

	wait for 10 us;
	dsp_rst <= '0';
	wait for 10 us;

	-- Mode ASK
	mux_data <= "00111001";
	sel_push <= '1';
	WAIT FOR 1 us;
	sel_push <= '0';
	wait for 20 us;
	
	mux_data <= "11000110";
	wait for 20 us;
	
	
	-- Mode QPSK
	mux_data <= "00111001";
	sel_push <= '1';
	WAIT FOR 1 us;
	sel_push <= '0';
	wait for 20 us;
	
	mux_data <= "11000110";
	wait for 20 us;

	-- Mode 8PSK
	mux_data <= "00111110";
	sel_push <= '1';
	WAIT FOR 1 us;
	sel_push <= '0';
	wait for 20 us;
	
	mux_data <= "00010011";
	wait for 20 us;
	
	mux_data <= "00001000";
	wait for 20 us;
	
	mux_data <= "00100101";
	wait for 20 us;
	
	wait for 500 us;
	
	stop <= true;
  
 end PROCESS stimulus;

END ARCHITECTURE;